#!/usr/bin/env python3  

"""A simple python script .

"""

import requests
import yaml

def download_yaml(url):
    response = requests.get(url)
    return response.text if response.status_code == 200 else None

def parse_yaml(yaml_text):
    try:
        parsed_data = yaml.safe_load(yaml_text)
        return parsed_data
    except yaml.YAMLError as e:
        print(f"Error parsing YAML: {e}")
        return None

def analyze_yaml(yaml_data):
    if not yaml_data or not isinstance(yaml_data, list):
        print("Invalid YAML data.")
        return

    # Number of elements in the list
    num_elements = len(yaml_data)
    print(f"Number of elements in the list: {num_elements}")

    # Parameters for each node
    if num_elements > 0 and isinstance(yaml_data[0], dict):
        parameters = yaml_data[0].keys()
        print(f"Parameters for each node: {', '.join(parameters)}")
    else:
        print("No parameters found for each node.")

def main():
    yaml_url = "https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/features.yml"
    
    yaml_text = download_yaml(yaml_url)
    if yaml_text:
        yaml_data = parse_yaml(yaml_text)
        analyze_yaml(yaml_data)

if __name__ == '__main__':
    main()