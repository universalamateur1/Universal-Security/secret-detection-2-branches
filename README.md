# Secret Detection 2 branches



## **Check if this is expected behavior:**

I have set up a project to recreate behavior of a Security ology Scan Execution and Result Policy interaction that seems strange.

Project is [https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches](https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches) (I can make anyone owner if they want) and is under the Security Policy Project inherited by the Group ([https://gitlab.com/universalamateur1/Universal-Security/appsec-group/appsec-security-policy-project](https://gitlab.com/universalamateur1/Universal-Security/appsec-group/appsec-security-policy-project) ).

We will focus on Secret Detection which is executed and the Scan Result policy should block when secrets are present in the MR, but not on the merge target.



1. Now I set up a Project with a Mann Branch. 
2. The Project has 2 or more feature Branches.
3. Secret Detection is activated with Scan Execution and Result Policy, as explained above.
4. In one feature branch ([https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches/-/blob/Temp-Env-01/config.ci.yaml?ref_type=heads](https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches/-/blob/Temp-Env-01/config.ci.yaml?ref_type=heads) ) is one file present, which will not be present at all on any other branch and there is a secret in that file.
5. In the other feature branch ([https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches/-/blob/feature/test-01/app.py](https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches/-/blob/feature/test-01/app.py) ) I have added a Secret to a general available file.
6. I Open a MR ([https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches/-/merge_requests/1](https://gitlab.com/universalamateur1/Universal-Security/secret-detection-2-branches/-/merge_requests/1) ) will be opened form the second branch to the main branch.
7. The MR is blocked by both secrets on both Features Branches, even if the file with one of the secrets is not present in the MR.
8. The FIndings do not show that the file is only present on the one branch, only the deep link goes to the feature branch, which does not happen on self managed installations.
* Questions: \
Is it expected behavior that a MR is blocked by a secret in a file which is not present in the MR?
* Can we add a line to the vulnerability modal which display when the found vulnerability is present on only one feature branch?
